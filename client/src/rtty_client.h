
#ifndef RTTY_CLIENT_H
#define RTTY_CLIENT_H

#include "../../lib/error_handler.h"
#include "../../lib/connection.h"
#include "client_com.h"

int 
start_client(int, char *);

#endif
