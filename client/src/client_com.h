#ifndef CLIENT_COM_H
#define CLIENT_COM_H

#include <fcntl.h> 
#include <sys/select.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "../../lib/error_handler.h"
#include "../../lib/connection.h"

#define BUFSIZE     1024

int 
client_com(int);

#endif
