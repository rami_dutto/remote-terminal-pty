
#include "rtty_client.h"

int 
start_client(int port, char *ipv4_addr)
{
    int                 peer_sock_fd;
    struct sockaddr_in  server_struct;
    int                 resp_ctrl;
    socklen_t           struct_len;


    //Create tcp ipv4 socket
    peer_sock_fd = ipv4_socket(TCP, PORT_NO_REBIND);
    
    //Configure peer tcp ipv4 struct
    server_struct = ipv4_struct(port, ipv4_addr);

    struct_len = sizeof(server_struct);

    //conect socket with peer socker
    resp_ctrl = connect(peer_sock_fd, (struct sockaddr*)&server_struct, struct_len);
    if (resp_ctrl == -1) {
        die("rtty_server.c:start_client():connect()");
    }

    client_com(peer_sock_fd);

    return 0;
}
