#include "rtty_client.h"
#include "./../../lib/rtty_getopt.c"

int main(int argc, char **argv){
    int port = 0;
    char host[15];

    serv_getopt(argc, argv, &port, host);
    
    start_client(port, host);

    return 0;
}
