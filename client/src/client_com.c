#include "client_com.h"

int 
client_com(int sock_fd)
{
    int             resp_ctrl;
    int             read_b;  
    int             write_b;     
    fd_set          read_fds;
    fd_set          write_fds;
    int             nfds;
    int             ready;
    char            buf[BUFSIZE];
    struct timeval  *tv;
    
    nfds = sock_fd + 1;
    
    FD_ZERO(&write_fds);
    FD_ZERO(&read_fds);
    tv = (struct timeval*)calloc(1, sizeof(struct timeval));
    tv->tv_sec = 0;
    tv->tv_usec = 0;

    while (1) {
        FD_SET(STDIN_FILENO, &read_fds);
        FD_SET(sock_fd, &read_fds);

        FD_SET(STDOUT_FILENO, &write_fds);
        FD_SET(sock_fd, &write_fds);

        ready = select(nfds, &read_fds, &write_fds, NULL, NULL);

        if (ready > -1) {
            if (FD_ISSET(sock_fd, &read_fds)) {

                write_b = recv(sock_fd, buf, BUFSIZE, 0);

                resp_ctrl = close_conn(buf);
                if (resp_ctrl == 0) {
                    write(STDOUT_FILENO, "disconnected\n", strlen("disconnected\n"));
                    resp_ctrl = close(sock_fd);
                    if(resp_ctrl == -1) {
                        die("client_com.c:client_comm():close()");
                    }
                    return EXIT_SUCCESS;
                }
                
                write(STDOUT_FILENO, buf, write_b);
                bzero(buf, BUFSIZE);
            }
            if (FD_ISSET(STDIN_FILENO, &read_fds)) {
                read_b = read(STDIN_FILENO, buf, BUFSIZE);

                send(sock_fd, buf, read_b, 0);

                bzero(buf, BUFSIZE);
            }
        }
    }

    return 0;
}
