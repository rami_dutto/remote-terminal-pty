/*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
| DESCRIPTION: Module that create an TCP coennction over IPv4                 |
| DATE: 26 Feb 2019                                                           |
| AUTHOR: Dutto Luquez, Ramiro                                                |
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/

#include "connection.h"

//TCP IPv4 socket creation (server & client)
int 
ipv4_socket(const int proto, const int rebind) 
{
    int new_sock;
    int resp_ctrl;
    //TCP Socket creation
    if (proto == TCP)
        new_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (proto == UDP)
        new_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (new_sock == -1)
        die("connection.c:tcp_socket():socket() = ");

    //server port rebind
    if (rebind) {
        int val = 1;
        int val_sz = sizeof(val);

        resp_ctrl = setsockopt(new_sock, SOL_SOCKET, SO_REUSEADDR, &val, val_sz);
        if (resp_ctrl == -1)
            die("connection.c:tcp_socket():setsockopt = ");
    }

    return new_sock;
}

//Set TCP IPv4 Struct (server & client)
struct sockaddr_in 
ipv4_struct(int port, char* ip_addr)
{
    struct sockaddr_in conn_struct;

    conn_struct.sin_family = PF_INET;     //AF_INET family
    conn_struct.sin_port = htons(port);   //Port number
    if (ip_addr) 
    {
        if (inet_aton(ip_addr , &(conn_struct.sin_addr)) == 0){
            conn_struct.sin_addr.s_addr = htonl(INADDR_ANY); 
        } 
    } else {
        conn_struct.sin_addr.s_addr = htonl(INADDR_LOOPBACK);  
    }

    return conn_struct;
}

int 
close_conn(char *action)
{
    if(strcmp(action, "close\n") != 0) {
        return -1;
    }

    return 0;
}
