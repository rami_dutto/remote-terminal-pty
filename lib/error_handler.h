#ifndef ERROR_HANDLER_H
#define ERROR_HANDLER_H

#include <stdlib.h>
#include <sys/errno.h>
#include <stdio.h>
#include <unistd.h>

void 
die(char *);

void 
alert(char *);

#endif
