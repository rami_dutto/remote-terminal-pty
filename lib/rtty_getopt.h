#ifndef RTTY_GETOPT_H
#define RTTY_GETOPT_H

#include <stdlib.h>
#include <stdio.h>
#include <getopt.h>
#include <ctype.h>
#include <string.h>

void
serv_getopt(int, char **, int *, char *);

void 
help(char *);

#endif
