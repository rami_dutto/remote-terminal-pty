#ifndef TCP_IPV4_CONN_H
#define TCP_IPV4_CONN_H

#define PORT_NO_REBIND  0
#define PORT_REBIND     1
#define UDP             0
#define TCP             1
#define COMM_SIZE       1024

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h> //struct sockaddr_in
#include <sys/errno.h>
#include <unistd.h>
#include <strings.h>
#include <string.h>


#include "error_handler.h"

/*----------------------------------------------------------------------------*
 *-------------------Function for TCP and UDP connections---------------------*
 *------------It can be used to creater either client or server---------------*
 *---------------------------------------------------------------------------*/
int
ipv4_socket(const int, const int);   // TCP or UDP IPv4 socket creation

struct sockaddr_in 
ipv4_struct(int, char*); // Set IPv4 Struct

int 
close_conn(char *);

#endif
