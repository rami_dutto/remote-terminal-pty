#include "rtty_getopt.h"

void
serv_getopt(int argc, char **argv, int *port, char *host) {
    int c;

    /* variables globales de getopt */
    extern char* optarg;
    extern int optind;
    extern int optopt;
    extern int opterr;
    opterr = 0;

    while ((c = getopt (argc, argv, "Hh:p:")) != -1)
        switch (c)
        {
        case 'p':
            *port = atoi(optarg);
            break;
        case 'h':
            strcpy(host, optarg);
            break;
        case 'H':
            help(argv[0]);
            break;
        case '?':
            if (strchr("hp", optopt) != NULL)
                fprintf (stderr, "-%c option require an argument.\n", optopt);
            else if (optopt=='?')
                help(argv[0]);
            else if (isprint (optopt))
                fprintf (stderr, "Unknown option'-%c'.\n", optopt);
            else
                fprintf (stderr, "Invalid character '\\x%x'.\n", optopt);
            help(argv[0]);
        default:
            exit(1);
        }

    /* Comprobamos los argumentos obligatorios */
    if (!(host)) {
        fprintf (stderr, "you must specify the HOST IP address in a mandatory way\n");
        help(argv[0]);
    }
    if (!(*port)) {
        fprintf (stderr, "you must specify the PORT in a mandatory way\n");
        help(argv[0]);
    }

}

void 
help(char *program)
{
  fprintf (stderr, "usage: %s -h host -p port\n\n", program);
  fprintf (stderr, "  -?, -H\t\tShow this help usage on the screen.\n");
  fprintf (stderr, "  -h host\t\tSpecified the ip\n");
  fprintf (stderr, "  -p port\t\tSpecified the port\n\n");
  exit (2);
}
