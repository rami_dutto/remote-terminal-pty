#include "error_handler.h"

void 
die(char *src)
{
    printf("errno: %d\n", errno);
    perror(src);
    exit(EXIT_FAILURE);
}

void 
alert(char *msg)
{
    printf("errno: %d\n", errno);
    perror(msg);
}
