#ifndef LOG_H
#define LOG_H

#include <arpa/inet.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <netinet/in.h> 

#include "../../lib/error_handler.h"

#define LOG           "./rttyclients.log"
#define COMMAND_SIZE  1024

typedef struct mapped
{
  pthread_mutex_t  *mutex;
  int              shm_fd;
  char             *mutex_name;
  int              log_fd;
  char             client_ip[INET_ADDRSTRLEN];
  char             buf[COMMAND_SIZE];
}mapped;

int 
open_log(mapped *);

void* 
th_log_write(void *);

char* 
string_time();

void
string_ip(struct sockaddr_in, char *);

#endif