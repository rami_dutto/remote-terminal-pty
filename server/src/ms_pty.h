#ifndef MS_PTY_H
#define MS_PTY_H

#define _XOPEN_SOURCE 700

#include <fcntl.h>
#include <sys/ioctl.h>
#include <stdlib.h>
#include <string.h>
#include <sys/poll.h>
#include <termios.h>
#include <string.h>
#include <strings.h>

#include "../../lib/error_handler.h"
#include "../../lib/connection.h"
#include "log.h"

#define SHELL       "/bin/bash"
#define MAX_SNAME   1000
#define BUFSIZE     1024


int 
ptym_open(char *, int);

int 
ptys_open(char *);

pid_t 
pty_fork(int *, char *, int , const struct termios *,
        const struct winsize *);

int 
spawn_shell(int, mapped *); 

int 
set_pty(int, mapped *);

#endif