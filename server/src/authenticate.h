#ifndef AUTHENTICATE_H
#define AUTHENTICATE_H

#define BUFFSIZE    516
#define MAX_LEN     256

 #include <fcntl.h>
#include <strings.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <string.h>

#include "../../lib/error_handler.h"

typedef struct peer_msg {
    long    mtype;
    int     clientId;
    char    mtext[BUFFSIZE];
} peer_msg;


char 
*get_login(int);

int 
usr_auth(int);

char *
read_line(FILE *);

int
validate_credentials(int, int, int);


#endif

