#include "authenticate.h"

char*
get_login(int com_sock_fd)
{
    int     resp_ctrl;
    char    *usr_name;
    char    *usr_pass;
    char    *buf = (char*)calloc(BUFFSIZE, sizeof(char));    

    bzero(buf, BUFFSIZE);

    //read client usrname
    resp_ctrl = write(com_sock_fd, "username: ", 10);
    if (resp_ctrl > MAX_LEN)
    {
        write(com_sock_fd, "username too long.\n", 33);
        write(com_sock_fd, "Disconnected from server.\n", 26);
        close(com_sock_fd);
        exit(EXIT_FAILURE);
    }
    usr_name = (char*)calloc(resp_ctrl - 1, sizeof(char));
    resp_ctrl = read(com_sock_fd, buf, resp_ctrl);
    memcpy(usr_name, buf, resp_ctrl);
    *(usr_name + resp_ctrl - 1) = '\0';
    //end usrname

    bzero(buf, BUFFSIZE);

    //read client password
    resp_ctrl = write(com_sock_fd, "password: ", 10);
    if (resp_ctrl > MAX_LEN)
    {
        write(com_sock_fd, "password too long.\n", 33);
        write(com_sock_fd, "Disconnected from server.\n", 26);
        close(com_sock_fd);
        exit(EXIT_FAILURE);
    }
    usr_pass = (char*)calloc(resp_ctrl - 1, sizeof(char));
    resp_ctrl = read(com_sock_fd, buf, resp_ctrl);
    memcpy(usr_pass, buf, resp_ctrl);
    *(usr_pass + resp_ctrl - 1) = '\0';
    //end password

    bzero(buf, BUFFSIZE);

    strcat(buf, "u:");
    strcat(buf, usr_name);
    strcat(buf, "-p:");
    strcat(buf, usr_pass);

    free(usr_name);
    free(usr_pass);

    return buf;
}

int 
usr_auth(int msqid)
{
    int         resp_ctrl;
    peer_msg    pm;
    FILE        *fin;
    char        *line;
    int         pid;

    pid = getpid();
    
    while (1) {
        //receive messages where message type is equal 1
        resp_ctrl = msgrcv(msqid, &pm, BUFFSIZE, pid, MSG_NOERROR);
        if (resp_ctrl == -1)
        {
            alert("usr_auth():msgrcv()");
        }
        else if (resp_ctrl > 0)
        {   
            pm.mtype = pm.clientId;
            fin = fopen("./credentials", "r");
            if (fin == NULL)
                alert("usr_auth():fopen()");
            while ((line = read_line(fin))) 
            {
                if (strstr(line, pm.mtext))
                {
                    bzero(pm.mtext, BUFFSIZE);
                    memcpy(pm.mtext, "ok", 2);
                    msgsnd(msqid, &pm, BUFFSIZE, 0);
                    break;
                }
            }
            fclose(fin);
            bzero(pm.mtext, BUFFSIZE);
            memcpy(pm.mtext, "denied", 7);
            msgsnd(msqid, &pm, BUFFSIZE, 0);
        }
    }
    exit(EXIT_SUCCESS);
}


char* 
read_line(FILE *fin)
{
    char    *buffer;
    char    *tmp;
    int     read_chars;
    char    *line = NULL;

    read_chars = 0;
    line = (char*)malloc(BUFFSIZE);

    if (!line) {
        return NULL;
    }

    buffer = line;

    while ( fgets(buffer, BUFFSIZE - read_chars, fin) ) {
        read_chars = strlen(line);

        if ( line[read_chars - 1] == '\n' ) {
            line[read_chars - 1] = '\0';
            return line;
        } else {
            tmp = realloc(line, BUFFSIZE * 2);
            if ( tmp ) {
                line = tmp;
                buffer = line + read_chars;
            } else {
                free(line);
                return NULL;
            }
        }
    }

    return NULL;
}

int 
validate_credentials(int sock_fd, int msqid, int msq_type) 
{
    peer_msg pm;
    int resp_ctrl;

    bzero(pm.mtext, BUFFSIZE);
    strcat(pm.mtext, get_login(sock_fd));
    pm.mtype = msq_type;
    pm.clientId = getpid();

    //send msq to auth fork
    resp_ctrl = msgsnd(msqid, &pm, BUFFSIZE, 0);
    if (resp_ctrl == -1) {   
        send(sock_fd, "Server error. Please reconnect and try again.\n", 46, 0);
        close(sock_fd);
        die("p2p_server.c:start_server():fork(2):msgsnd()");
    }

    //receive response from auth fork
    //receive messages where message type is equal to my process ID
    resp_ctrl = msgrcv(msqid, &pm, BUFFSIZE, pm.clientId, MSG_NOERROR);
    if (resp_ctrl == -1) {
        send(sock_fd, "Server error. Please reconnect and try again.\n", 46, 0);
        close(sock_fd);
        die("p2p_server.c:start_server():fork(2):msgrcv()");
    }

    if (strcmp(pm.mtext, "ok") == 0) {
        return 0;
    } else {
        return -1;
    }
}

