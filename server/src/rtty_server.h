
#ifndef RTTY_SERVER_H
#define RTTY_SERVER_H

#include <unistd.h>

#include "authenticate.h"
#include "log.h"
#include "ms_pty.h"
#include "../../lib/connection.h"
#include "../../lib/error_handler.h"

int 
start_server();

#endif
