#include "rtty_server.h"

int 
start_server(int port, char *ipv4_addr)
{
	int 				serv_sock_fd;
    struct sockaddr_in 	server_struct;
    int 				resp_ctrl;
    int 				auth_pid;
    socklen_t 			struct_len;
    key_t 				key;
    int 				msqid;
    int 				com_sock;
    int 				msgflg;
    mapped              mapping;

    //Create tcp ipv4 socket
    serv_sock_fd = ipv4_socket(TCP ,PORT_REBIND);
    
    //Configure tcp ipv4 struct
    server_struct = ipv4_struct(port, ipv4_addr);

    //Bind socket and struct
    struct_len = sizeof(server_struct);
    resp_ctrl = bind(serv_sock_fd, (struct sockaddr*)&server_struct, struct_len);
    if (resp_ctrl == -1) {
        die("rtty_server.c:start_server():bind()");
    }

    //Listen connection (passive socket)
    resp_ctrl = listen(serv_sock_fd, 128);
    if (resp_ctrl == -1) {
        die("rtty_server.c:start_server():listen()");
    }

    //QUEUE
    //key for queue
    /* The ftok() function attempts to create a unique key */
    key = ftok(".", getpid());
    if (key == -1) {
        die("rtty_server.c:start_server():ftok()");
    }

    //queue creation
	msgflg = IPC_CREAT | 0666;
    msqid = msgget(key, msgflg);
    if (msqid == -1) {
        die("rtty_server.c:start_server():msgget()");
    }
    
    //auth fork
    auth_pid = fork();
    if (auth_pid == -1) {
        die("rtty_server.c:start_server():fork(1)");

    } else if(auth_pid == 0) {
        usr_auth(msqid);

    } else {
        
        struct_len = sizeof((struct sockaddr*)&server_struct);
        resp_ctrl = open_log(&mapping);

        while (1) {
            //accept connection
            com_sock = accept(serv_sock_fd, (struct sockaddr*)&server_struct, &struct_len);
            if (com_sock == -1) {
                die("rtty_server.c:start_server():accept()");
            }

            string_ip(server_struct, mapping.client_ip);
            //communication fork
            resp_ctrl = fork();
            //fork fialed
            if (resp_ctrl == -1){
                send(com_sock, "Server error. Please reconnect and try again.\n", 46, 0);
                close(com_sock);
                die("rtty_server.c:start_server():fork(2)");
            }
            //child pid
            if (resp_ctrl == 0) {
                /*child body*/
                resp_ctrl = validate_credentials(com_sock, msqid, auth_pid);
                //rtty access grant
                if(resp_ctrl == 0) {
                    //Thread log
                    //Main thread mannage comm
					spawn_shell(com_sock, &mapping);
                    resp_ctrl = close(com_sock);
                    if (resp_ctrl == -1) {
                        die("rtty_server.c:spawn_shell():close()");
                    }
                    exit(EXIT_SUCCESS);

                }
                //rtty access deniend
                if (resp_ctrl == -1) {
                    send(com_sock, "connection refused.\n", 20, 0);
                    close(com_sock);
                    exit(EXIT_SUCCESS);
                }
            } else {
                //Socker no nedded on parent after fork
                close(com_sock);
            }
        }
    }

    close(serv_sock_fd);
    close(mapping.log_fd);

    /* Clean up. */
    pthread_mutex_destroy(mapping.mutex);
    shm_unlink("/shared_mutex");

    return 0;
}
