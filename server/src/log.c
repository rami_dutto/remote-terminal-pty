#include "log.h"

int 
open_log(mapped *mapping)
{
    pthread_mutexattr_t mutexattr;
    int                 resp_ctrl;
    void                *addr;
    
    /* shm_open  crea  y  abre  un  nuevo,  o  abre  uno  ya  existente,  objeto POSIX de memoria
       compartida.  Un objeto POSIX de memoria compartida es en efecto un manejador que puede ser
       utilizado  por  procesos  no  relacionados para ubicar la misma zona de memoria compartida
       mediante mmap(2).*/
    mapping->shm_fd = shm_open("/shared_mutex", O_RDWR|O_CREAT , 0660);
    if (mapping->shm_fd == -1) {
        die("shm_open");
    }
    /* Las  funciones truncate y ftruncate hacen que el fichero regular cuyo nombre es path o que
       es referenciado por fd sea truncado a un tamaño de length bytes. */
    if (ftruncate(mapping->shm_fd, sizeof(pthread_mutex_t)) != 0) {
        die("ftruncate");
    }
    // Map pthread mutex into the shared memory.
    addr =  mmap( NULL, sizeof(pthread_mutex_t), PROT_READ|PROT_WRITE, MAP_SHARED, mapping->shm_fd, 0 );
    if (addr == MAP_FAILED) {
        die("mmap");
    }
    mapping->mutex = (pthread_mutex_t *)addr;


    // Open log file
    mapping->log_fd = open(LOG, O_CREAT | O_RDWR | O_APPEND, 0777);
    if (mapping->log_fd  == -1)
    {
        die("log.c:open_log():open()");
    }
    
    //initialize default mutex attributes
    /* Storage for each attribute object is allocated by the threads system during 
     * execution. mutexattr is an opaque type that contains a system-allocated attribute 
     * object. The possible values of mutexattr's scope are PTHREAD_PROCESS_PRIVATE (the 
     * default) and PTHREAD_PROCESS_SHARED.The default value of the pshared attribute 
     * when this function is called is PTHREAD_PROCESS_PRIVATE, which means that the 
     * initialized mutex can be used within a process.*/
    resp_ctrl = pthread_mutexattr_init(&mutexattr);
    if (resp_ctrl != 0)
        die("log.c:open_log():pthread_mutexattr_init()");

    /* The scope of a mutex variable can be either process private (intraprocess) 
     * or system wide (interprocess). The function pthread_mutexattr_setpshared() is 
     * used to set the scope of a mutex atrribute. If the mutex is created with the 
     * pshared (POSIX) attribute set to the PTHREAD_PROCESS_SHARED state, and it 
     * exists in shared memory, it can be shared among threads from more than one 
     * process.*/
    resp_ctrl = pthread_mutexattr_setpshared(&mutexattr, PTHREAD_PROCESS_SHARED);
    if (resp_ctrl != 0)
        die("log.c:open_log():pthread_mutexattr_setpshared()");
    resp_ctrl = pthread_mutex_init(mapping->mutex, &mutexattr);
    if (resp_ctrl != 0)
        die("log.c:open_log():pthread_mutex_init()");

    return EXIT_SUCCESS;
}

void* 
th_log_write(void *arg)
{
    mapped *mapping = (mapped*)arg;
    char *time = calloc(1, sizeof(char));
    time = string_time();
    //int client_ip_len = strlen(mapping->client_ip);
    pthread_mutex_lock(mapping->mutex);

        write(mapping->log_fd, "(", strlen("("));
        write(mapping->log_fd, time, strlen(time) - 1);
        write(mapping->log_fd, " from IP: ", strlen(" from IP: "));
        write(mapping->log_fd, mapping->client_ip, strlen(mapping->client_ip));
        write(mapping->log_fd, ")", strlen(")"));
        write(mapping->log_fd, ":", strlen(":"));
        write(mapping->log_fd, mapping->buf, strlen(mapping->buf));

    pthread_mutex_unlock(mapping->mutex);
 
    pthread_exit(NULL);
}

char*
string_time() {
    time_t current_time;
    char* c_time_string;

    /* Obtain current time. */
    current_time = time(NULL);

    if (current_time == ((time_t)-1))
    {
        (void) fprintf(stderr, "Failure to obtain the current time.\n");
        exit(EXIT_FAILURE);
    }

    /* Convert to local time format. */
    c_time_string = ctime(&current_time);

    if (c_time_string == NULL)
    {
        (void) fprintf(stderr, "Failure to convert the current time.\n");
        exit(EXIT_FAILURE);
    }

    return c_time_string;
}

void
string_ip(struct sockaddr_in client_addr, char *buf){
    struct sockaddr_in* pV4Addr = (struct sockaddr_in*)&client_addr;
    struct in_addr ipAddr = pV4Addr->sin_addr;

    inet_ntop( AF_INET, &ipAddr, buf, INET_ADDRSTRLEN );
}
 