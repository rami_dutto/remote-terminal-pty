#include "ms_pty.h"

// Open master pty and find free slave pty name
int 
ptym_open(char *pts_name, int pts_namesz)
{
    int     master_fd;
    int     resp_ctrl;
    char    *p;

    //Open pty master
    master_fd = posix_openpt(O_RDWR | O_NOCTTY);
    if (master_fd == -1) {
        return -1;
    }

    //Grant access to pty slave
    resp_ctrl = grantpt(master_fd);
    if (resp_ctrl == -1) {
        alert("ms_pty.c:ptym_open():grantpt()");
        close(master_fd);
        return -1;
    }

    /*Unlosck slave pty*/
    resp_ctrl = unlockpt(master_fd);
    if (resp_ctrl == -1) {
        alert("ms_pty.c:ptym_open():unlockpt()");
        close(master_fd);
        return -1;
    }

    //Get slave pty name
    p = ptsname(master_fd);
    if (p == NULL) {
        alert("ms_pty.c:ptym_open():ptsname()");
        close(master_fd);
        return -1;
    }

    if (strlen(p) < pts_namesz) {
        strncpy(pts_name, p, pts_namesz);
    } else {
        alert("ms_pty.c:ptym_open()-pts_namesz too small");
        close(master_fd);
        return -1;
    }
    return master_fd;
}

//open a fd for slave pty name
int 
ptys_open(char *pts_name)
{
    int fds;

    /* open slave fd based on the availabel slave name */
    fds = open(pts_name, O_RDWR);
    if (fds == -1) {
        die("ms_pty.c:ptys_open():ptsname()");
    }

    return fds;
}

pid_t 
pty_fork (int *ptrfdm, char *slave_name, int slave_namesz, 
          const struct termios *slave_termios, 
          const struct winsize *slave_winsize)
{
    int     master_fd; 
    int     slave_fd;
    pid_t   pid;
    char    pts_name[MAX_SNAME];
    int     resp_ctrl;

    /* Open master fd and set available slave name*/
    master_fd = ptym_open(pts_name, MAX_SNAME);
    if (master_fd == -1){
        alert("ms_pty.c:ptym_open():ptym_open()");
        return -1;
    }

    if (slave_name != NULL) {
        /* Return name of slave.  Null terminate to handle case
         * where strlen(pts_name) > slave_namesz. */
        strncpy(slave_name, pts_name, slave_namesz);
    }

    /* Child process for mannage the slave. */
    pid = fork();
    if (pid == -1) {
        return -1;
    } 

    //START parent code after fork
    if (pid > 0) {
        *ptrfdm = master_fd;  /* return fd of master */

        return pid ;    /* parent returns pid of child */
    }
    //END parent code
    
    //START child code
    /* From here the master_fd is duplicated so we ned to close
     * it on the child.*/
    resp_ctrl = close(master_fd); //no nedded in the child
    if (resp_ctrl == -1) {
        alert("ms_pty.c:pty_fork():close(slave_fd)");
    }
    
    //Start a new sessión where de child is the leader
    resp_ctrl = setsid();
    if (resp_ctrl == -1) {
        alert("setsid error");
        return -1;
    }

    // Child acquires controlling terminal on open().
    slave_fd = ptys_open(slave_name);
    if (slave_fd == -1) {
        alert("can’t open slave pty");
        return -1;
    }

    #ifdef TIOCSCTTY
        //TIOCSCTTY is the BSD way to acquire a controlling terminal.
        resp_ctrl = ioctl(slave_fd, TIOCSCTTY, (char *)0);
        if (resp_ctrl == -1) {
            alert("TIOCSCTTY error");
            return -1;
        }
    #endif

    // Set slave tty attributes.
    if (slave_termios != NULL) {
        resp_ctrl = tcsetattr(slave_fd, TCSANOW, slave_termios);
        if (resp_ctrl == -1) {
            alert("tcsetattr error on slave pty");
            return -1;
        }
    }

    // Set slave tty window size.
    if (slave_winsize != NULL) {
        resp_ctrl = ioctl(slave_fd, TIOCSWINSZ, slave_winsize);
        if (resp_ctrl == -1) {
            alert("TIOCSWINSZ error on slave pty");
            return -1;
        }
    }

    // Duplicate pty slave fd's to be child's stdin/stdout/stderr.
    resp_ctrl = dup2(slave_fd, STDIN_FILENO);
    if (resp_ctrl != STDIN_FILENO) {
        alert("ms_pty.c:pty_fork():dup2()-STDIN_FILENO");
        return -1;
    } 
    resp_ctrl = dup2(slave_fd, STDOUT_FILENO);
    if (resp_ctrl != STDOUT_FILENO) {
        alert("ms_pty.c:pty_fork():dup2()-STDOUT_FILENO");
        return -1;
    }
    resp_ctrl = dup2(slave_fd, STDERR_FILENO);
    if (resp_ctrl != STDERR_FILENO) {
        alert("ms_pty.c:pty_fork():dup2()-STDERR_FILENO");
        return -1;
    }
    
    if (slave_fd != STDIN_FILENO && slave_fd != STDOUT_FILENO &&
    slave_fd != STDERR_FILENO) {
        resp_ctrl = close(slave_fd); //slave_fd no nedded after dup2()
        if (resp_ctrl == -1) {
            alert("ms_pty.c:pty_fork():close(slave_fd)");
        }
    }
    
    return EXIT_SUCCESS;
}


int 
spawn_shell(int com_sock, mapped *mapping) 
{
	int 			pid; 
	int 			master_fd;
	char 			slave_name[MAX_SNAME];
	struct termios 	tty;
	struct winsize 	ws;
    int             resp_ctrl;

    // Create the master pty and fork the slave pty
	pid = pty_fork(&master_fd, slave_name, MAX_SNAME, &tty, &ws);
	if (pid == 0){
        /* The child who have the slave pty spawn a terminal oriented 
         * programm*/
        resp_ctrl = execl(SHELL, SHELL, 0);
		if(resp_ctrl > 0) {
			die("cannot exec shell");
        }
		exit(0);
	}

    /* Redirect the stdin/stoud/stderr of the process that drive
     * the master pty to the socket*/
	resp_ctrl = dup2(com_sock, STDIN_FILENO);
    if (resp_ctrl != STDIN_FILENO) {
        alert("ms_pty.c:spawn_shell():dup2()-STDIN_FILENO");
        return -1;
    }

	resp_ctrl = dup2(com_sock, STDOUT_FILENO);
    if (resp_ctrl != STDOUT_FILENO) {
        alert("ms_pty.c:spawn_shell():dup2()-STDOUT_FILENO");
        return -1;
    }

	resp_ctrl = dup2(com_sock, STDERR_FILENO);
    if (resp_ctrl != STDERR_FILENO) {
        alert("ms_pty.c:spawn_shell():dup2()-STDERR_FILENO");
        return -1;
    }
	
    resp_ctrl = set_pty(master_fd, mapping);

    if(resp_ctrl == 0){
        send(STDOUT_FILENO, "close\n", strlen("close\n"), 0);
    }

    close(master_fd);
    
    return 0;
}

int 
set_pty(int master_fd, mapped *mapping)
{
	int             i;
    int             done=0;
	char            buf[BUFSIZE];
	struct pollfd   ufds[2];
	struct termios  ot; 
    struct termios  t;
    pthread_t       worker_thread;
    void            *arg;


    arg = (char*)mapping;

	tcgetattr(STDIN_FILENO, &ot);

	t =             ot;
	t.c_lflag &=    ~( ICANON | ISIG | ECHO | ECHOE | ECHONL );
	t.c_iflag |=    IGNBRK;

	tcsetattr(STDIN_FILENO, TCSANOW, &t);

	ufds[0].fd =        master_fd;
	ufds[0].events =    POLLIN;
	ufds[1].fd =        STDIN_FILENO;
	ufds[1].events =    POLLIN;

	while(!done) {

		int r = poll(ufds, 2, -1);

		if((r < 0) && (errno != EINTR)) {
			done = 1;
			break;
		}

		if ((ufds[0].revents | ufds[1].revents) & (POLLERR | POLLHUP | POLLNVAL)) {
			done = 1;
			break;
		}

		if(ufds[0].revents & POLLIN) {
			i = read(master_fd, buf, BUFSIZE);
            //write(STDIN_FILENO, "1\n", 2);
			if(i >=1) {
				write(STDOUT_FILENO, buf, i);
                //write(STDIN_FILENO, "2\n", 2);
			} else {
				done = 1;
			}
		}
		if(ufds[1].revents & POLLIN)
		{
			i = read(STDIN_FILENO, buf, BUFSIZE);
            //write(STDIN_FILENO, "3\n", 2);

			if(i >= 1) {
                //Acá va el thread que escribe en el log
                bzero(mapping->buf, COMMAND_SIZE);
                strncpy(mapping->buf, buf, i);
                if (pthread_create(&worker_thread, NULL, th_log_write, arg) != 0) {
                    die("pthread");
                }
				write(master_fd, buf, i);
                //write(STDIN_FILENO, "4\n", 2);
			} else {
				done =1;
			}
		}
	}

    return EXIT_SUCCESS;
}

