#include "rtty_server.h"
#include "./../../lib/rtty_getopt.h"

int main(int argc, char **argv){
    int port = 0;
    char host[15];

    serv_getopt(argc, argv, &port, host);

    start_server(port, host);

    return 0;
}