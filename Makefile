# IMPORTANT variables
SHAREDSRC = lib/rtty_getopt.c
SHAREDLIB = lib/
SERVERSRCDIR = server/src/
CLIENTSRCDIR = client/src/

#locations
OUTDIR = bin/

CC = gcc
CLIBS = -IPTHREAD
WARNINGS = -W -Wall -Wextra -pedantic -Wstrict-overflow=5 -Wshadow -Wpointer-arith -Wcast-qual -Wstrict-prototypes

#include "rtty_server.h"
#include "./../../lib/rtty_getopt.h"
server: server/src/rtty_server.o $(SHAREDSRC)
	$(CC) $(WARNINGS) $(CLIBS) $< -o $(OUTDIR)$@



lib/error_handler.o: lib/error_handler.c
	$(CC) -c $< -o $@

lib/connection.o: lib/connection.c lib/error_handler.h 
	$(CC) -c -I$(SHAREDLIB) $< -o $@

lib/rtty_getopt.o: lib/rtty_getopt.c
	$(CC) -c -I$(SHAREDLIB) $< -o $@



server/src/authenticate.o: server/src/authenticate.c lib/error_handler.h
	$(CC) -c -I$(SHAREDLIB) $< -o $@

server/src/log.o: server/src/log.c lib/error_handler.h
	$(CC) -c -I$(SHAREDLIB) $< -o $@

server/src/ms_pty.o: server/src/ms_pty.c server/src/log.h lib/connection.h lib/error_handler.h
	$(CC) -c -I$(SHAREDLIB) -I$(SERVERSRCDIR) $< -o $@

server/src/rtty_server.o: server/src/rtty_server.c lib/rtty_getopt.h
	$(CC) -c -I$(SHAREDLIB) $< -o $@
	
server/src/server.o: server/src/server.c server/src/rtty_server.h lib/rtty_getopt.h
	$(CC) -c  -I$(SHAREDLIB) -I$(SERVERSRCDIR) $< -o $@



client/src/client_com.o: client/src/client_com.c lib/error_handler.h lib/connection.h
	$(CC) -c -I$(SHAREDLIB) $< -o $@

client/src/rtty_client.o: client/src/rtty_client.c client/src/client_com.h lib/connection.h lib/error_handler.h 
	$(CC) -c -I$(SHAREDLIB) -I$(CLIENTSRCDIR) $< -o $@

client/src/client.o: client/src/client.c client/src/rtty_client.h lib/rtty_getopt.h
	$(CC) -c  -I$(SHAREDLIB) -I$(CLIENTSRCDIR) $< -o $@




.PHONY: clean all client
all: 
	- make server && make client && chmod a+x $(OUTDIR)*
clean:
	- rm $(OUTDIR)* && rm $(CLIENTSRCDIR)*.o
client: 
	gcc -Wall -o client client/src/client_com.c client/src/client.c client/src/rtty_client.c lib/connection.c lib/error_handler.c

#-include ./server/Makefile ./client/Makefile
# DO NOT DELETE

lib/connection.o: lib/connection.h lib/error_handler.h
lib/error_handler.o: lib/error_handler.h
lib/rtty_getopt.o: lib/rtty_getopt.h
